(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
  typeof define === 'function' && define.amd ? define(['exports', '@angular/core'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.ng = {}, global.i0));
})(this, (function (exports, i0) { 'use strict';

  function _interopNamespace(e) {
    if (e && e.__esModule) return e;
    var n = Object.create(null);
    if (e) {
      Object.keys(e).forEach(function (k) {
        if (k !== 'default') {
          var d = Object.getOwnPropertyDescriptor(e, k);
          Object.defineProperty(n, k, d.get ? d : {
            enumerable: true,
            get: function () { return e[k]; }
          });
        }
      });
    }
    n["default"] = e;
    return Object.freeze(n);
  }

  var i0__namespace = /*#__PURE__*/_interopNamespace(i0);

  class MfService {
    constructor() {}

  }

  MfService.ɵfac = function MfService_Factory(t) {
    return new (t || MfService)();
  };

  MfService.ɵprov = /* @__PURE__ */i0__namespace.ɵɵdefineInjectable({
    token: MfService,
    factory: MfService.ɵfac,
    providedIn: 'root'
  });

  (function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0__namespace.ɵsetClassMetadata(MfService, [{
      type: i0.Injectable,
      args: [{
        providedIn: 'root'
      }]
    }], function () {
      return [];
    }, null);
  })();

  class MfComponent {
    constructor() {}

    ngOnInit() {}

  }

  MfComponent.ɵfac = function MfComponent_Factory(t) {
    return new (t || MfComponent)();
  };

  MfComponent.ɵcmp = /* @__PURE__ */i0__namespace.ɵɵdefineComponent({
    type: MfComponent,
    selectors: [["lib-mf"]],
    decls: 2,
    vars: 0,
    template: function MfComponent_Template(rf, ctx) {
      if (rf & 1) {
        i0__namespace.ɵɵelementStart(0, "p");
        i0__namespace.ɵɵtext(1, " mf works! ");
        i0__namespace.ɵɵelementEnd();
      }
    },
    encapsulation: 2
  });

  (function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0__namespace.ɵsetClassMetadata(MfComponent, [{
      type: i0.Component,
      args: [{
        selector: 'lib-mf',
        template: `
    <p>
      mf works!
    </p>
  `,
        styles: []
      }]
    }], function () {
      return [];
    }, null);
  })();

  class MfModule {
    constructor() {
      console.debug('Hello from MF');
    }

  }

  MfModule.ɵfac = function MfModule_Factory(t) {
    return new (t || MfModule)();
  };

  MfModule.ɵmod = /* @__PURE__ */i0__namespace.ɵɵdefineNgModule({
    type: MfModule
  });
  MfModule.ɵinj = /* @__PURE__ */i0__namespace.ɵɵdefineInjector({
    imports: [[]]
  });

  (function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0__namespace.ɵsetClassMetadata(MfModule, [{
      type: i0.NgModule,
      args: [{
        declarations: [MfComponent],
        imports: [],
        exports: [MfComponent]
      }]
    }], function () {
      return [];
    }, null);
  })();

  exports.MfComponent = MfComponent;
  exports.MfModule = MfModule;
  exports.MfService = MfService;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
