import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as AngularCore from '@angular/core';

const routes: Routes = [{
  path: 'mf',
  loadChildren: () => {
    const SystemJS = (window as any).System;

    // SystemJS.constructor.prototype.createScript = (url: string) => {
    //   const script = document.createElement('script');
    //   script.src = url;
    //   script.type = 'module';
    //   return script;
    // }

    SystemJS.set("app:@angular/core", AngularCore);

    return SystemJS.import('http://localhost:4200/assets/mf.umd.js')
      .then(m => m.MfModule);
  }
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
