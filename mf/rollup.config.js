export default {
  input: 'dist/mf/mf.mjs',
  output: {
    file: 'dist/mf/umd/mf.umd.js',
    format: 'umd',
    name: 'ng'
  }
}
