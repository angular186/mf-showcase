import { NgModule } from '@angular/core';
import { MfComponent } from './mf.component';

@NgModule({
  declarations: [
    MfComponent
  ],
  imports: [
  ],
  exports: [
    MfComponent
  ]
})
export class MfModule {
  constructor() {
    console.debug('Hello from MF');
  }
}
